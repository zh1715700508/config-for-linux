#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias la='ls -la'
alias ll='ls -l'
alias psaux='ps -aux'
alias ra='ranger'
# alias del='mv $@ b'
alias del='sudo mv -t /root/.local/share/Trash/files --backup=t'
PS1='[\u@\h \W]\$ '
# export MYVIMRC=$MYVIMRC:/home/zero/.vim/vimrc

